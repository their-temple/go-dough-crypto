# Go Dough Crypto

Aka Godot-Crypto, an implementation of [Bitcoin](https://bitcoin.org/bitcoin.pdf) in Godot

Possibly support [other use cases](https://github.com/LibertyDSNP/papers/blob/main/whitepaper/dsnp_whitepaper.pdf)
